[![GoDoc](https://godoc.org/gitlab.com/flimzy/fbconvert?status.png)](http://godoc.org/gitlab.com/FlashbackSRS/fbconvert)

A utility to read an Anki *.apkg file and produce a Flashback-consumable data structure. Licensed under the AGPLv3.
