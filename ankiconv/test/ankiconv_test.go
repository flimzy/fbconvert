package test

import (
	"testing"
	"time"

	"gitlab.com/flimzy/testy"

	"github.com/flimzy/anki"
	"gitlab.com/FlashbackSRS/fbconvert/ankiconv"
	fbmodel "gitlab.com/FlashbackSRS/flashback-model"
)

const ApkgFile = "Art.apkg"

var UUID = []byte{0xD1, 0xC9, 0x58, 0x7D, 0x88, 0xDF, 0x4A, 0x65, 0x89, 0x23, 0xF7, 0x3C, 0xDF, 0x6D, 0x1D, 0x70}
var now = time.Unix(1469977704, 730156517).UTC()

func TestConvert(t *testing.T) {
	apkg, err := anki.ReadFile(ApkgFile)
	if err != nil {
		t.Fatal(err)
	}

	u := fbmodel.NewUser(fbmodel.B32enc(UUID))

	b := ankiconv.NewBundle()
	b.SetNow(now)
	err = b.Convert("Art", u, apkg)
	if err != nil {
		t.Fatal(err)
	}

	if d := testy.DiffAsJSON(testy.Snapshot(t), b); d != nil {
		t.Error(d)
	}
}
