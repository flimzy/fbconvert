package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/flimzy/anki"
	"gitlab.com/FlashbackSRS/fbconvert/ankiconv"
	fbmodel "gitlab.com/FlashbackSRS/flashback-model"
	"gitlab.com/FlashbackSRS/flashback-model/impexp"
)

var inputFile = flag.String("input", "-", "Input filename")
var outputFile = flag.String("output", "", "Output filename")

func init() {
	flag.Parse()
}

func main() {
	var apkg *anki.Apkg
	var err error
	if *inputFile == "-" {
		var content []byte
		content, err = ioutil.ReadAll(os.Stdin)
		if err == nil {
			apkg, err = anki.ReadBytes(content)
		}
	} else {
		apkg, err = anki.ReadFile(*inputFile)
	}
	if err != nil {
		panic(err)
	}
	defer apkg.Close() // nolint: errcheck
	if cards, e := apkg.Cards(); e != nil {
		fmt.Printf("Error reading cards: %s\n", e)
	} else {
		var count int
		for cards.Next() {
			count++
		}
		fmt.Printf("Found %d cards\n", count)
	}

	u := fbmodel.NilUser()

	// The bundle
	bundleName := *inputFile
	if bundleName == "-" {
		bundleName = "Anki Collection"
	}
	bundle, err := ankiconv.Convert(bundleName, u, apkg)
	if err != nil {
		panic(err)
	}

	var out io.Writer
	if *outputFile == "" {
		if *inputFile == "-" {
			out = os.Stdout
		} else {
			f, err := os.Create(strings.TrimSuffix(*inputFile, ".apkg") + ".fbb")
			if err != nil {
				panic(err)
			}
			out = f
			defer f.Close() // nolint: errcheck
		}
	} else {
		f, err := os.Create(*outputFile)
		if err != nil {
			panic(err)
		}
		out = f
		defer f.Close() // nolint: errcheck
	}

	if err := impexp.Pack(out, bundle.Package); err != nil {
		panic(err)
	}
}
