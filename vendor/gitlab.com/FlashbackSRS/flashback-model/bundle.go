package fb

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/pkg/errors"
)

const (
	// TemplateContentType sets the content type of Flashback Template segments
	TemplateContentType = "text/html"
	// BundleContentType ses the content type of Flashback Bundles
	BundleContentType = "application/json"
)

// Bundle represents a Bundle database.
type Bundle struct {
	ID          string    `json:"_id"`
	Rev         string    `json:"_rev,omitempty"`
	Created     time.Time `json:"created"`
	Modified    time.Time `json:"modified"`
	Imported    time.Time `json:"imported,omitempty"`
	Owner       string    `json:"-"`
	Name        string    `json:"name,omitempty"`
	Description string    `json:"description,omitempty"`
}

// NewBundle creates a new Bundle with the provided id and owner.
func NewBundle(id, owner string) (*Bundle, error) {
	if id == "" {
		return nil, errors.New("id required")
	}
	b := &Bundle{
		ID:       "bundle-" + owner + "-" + id,
		Owner:    owner,
		Created:  nowUTC(),
		Modified: nowUTC(),
	}
	return b, nil
}

type bundleAlias Bundle

// MarshalJSON implements the json.Marshaler interface for the Bundle type.
func (b *Bundle) MarshalJSON() ([]byte, error) {
	type encBundle struct {
		bundleAlias
		Type     string     `json:"type"`
		Created  shortTime  `json:"created"`            // nolint: govet
		Modified shortTime  `json:"modified"`           // nolint: govet
		Imported *shortTime `json:"imported,omitempty"` // nolint: govet
	}

	doc := encBundle{
		Type:        "bundle",
		bundleAlias: bundleAlias(*b),
		Created:     shortTime{b.Created},
		Modified:    shortTime{b.Modified},
	}
	if !b.Imported.IsZero() {
		doc.Imported = &shortTime{b.Imported}
	}
	return json.Marshal(doc)
}

// UnmarshalJSON fulfills the json.Unmarshaler interface for the Bundle type.
func (b *Bundle) UnmarshalJSON(data []byte) error {
	doc := new(struct {
		bundleAlias
		Owner string `json:"owner"`
	})
	if err := json.Unmarshal(data, &doc); err != nil {
		return errors.Wrap(err, "failed to unmarshal Bundle")
	}
	*b = Bundle(doc.bundleAlias)
	parts := strings.SplitN(b.ID, "-", 3)
	if len(parts) == 3 {
		b.Owner = parts[1]
	}
	return nil
}

// SetRev sets the internal _rev attribute of the Bundle
func (b *Bundle) SetRev(rev string) { b.Rev = rev }

// DocID returns the document's ID as a string.
func (b *Bundle) DocID() string { return b.ID }

// ImportedTime returns the time the Bundle was imported, or nil
func (b *Bundle) ImportedTime() time.Time { return b.Imported }

// ModifiedTime returns the time the Bundle was last modified
func (b *Bundle) ModifiedTime() time.Time { return b.Modified }

// MergeImport attempts to merge i into b, returning true if a merge took place,
// or false if no merge was necessary.
func (b *Bundle) MergeImport(i interface{}) (bool, error) {
	existing := i.(*Bundle)
	if b.ID != existing.ID {
		return false, errors.New("IDs don't match")
	}
	if !b.Created.Truncate(time.Second).Equal(existing.Created.Truncate(time.Second)) {
		return false, errors.New("Created timestamps don't match")
	}
	if b.Owner != existing.Owner {
		return false, errors.New("Cannot change bundle ownership")
	}
	if b.Imported.IsZero() || existing.Imported.IsZero() {
		return false, errors.New("not an import")
	}
	b.Rev = existing.Rev
	if b.Modified.After(existing.Modified) {
		// The new version is newer than the existing one, so update
		return true, nil
	}
	// The new version is older, so we need to use the version we just read
	b.Name = existing.Name
	b.Description = existing.Description
	b.Modified = existing.Modified
	b.Imported = existing.Imported
	return false, nil
}

// DisplayName returns a human-consumable display name
func (b *Bundle) DisplayName() string {
	if b.Name != "" {
		return b.Name
	}
	return "{" + strings.TrimPrefix(b.ID, "bundle-") + "}"
}
