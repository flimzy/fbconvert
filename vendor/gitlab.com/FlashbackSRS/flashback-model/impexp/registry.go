package impexp

import (
	"fmt"
	"io"

	fb "gitlab.com/FlashbackSRS/flashback-model"
	"gitlab.com/FlashbackSRS/flashback-model/v2" // nolint: goimports
	"gitlab.com/FlashbackSRS/flashback-model/v3"
)

// Unpacker reads a package's JSON, and returns a parsed Package.
type Unpacker interface {
	Unpack(io.Reader) (*fb.Package, error)
}

var unpackers = map[string]Unpacker{
	"1": &v2.Unpacker{},
	"2": &v2.Unpacker{},
	"3": &v3.Unpacker{},
}

func unpacker(version string) (Unpacker, error) {
	unpacker, ok := unpackers[version]
	if !ok {
		return nil, fmt.Errorf("package version `%s` not supported", version)
	}
	return unpacker, nil
}
