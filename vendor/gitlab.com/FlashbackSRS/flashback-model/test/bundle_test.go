package test

import (
	"encoding/json"
	"testing"

	"github.com/flimzy/testify/require"
	"gitlab.com/flimzy/testy"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

var frozenBundle = []byte(`
{
	"_id": "bundle-tui5ajfbabaeljnxt4om7fwmt4-krsxg5baij2w4zdmmu",
	"type": "bundle",
	"created": "2016-07-31T15:08:24.730156517Z",
	"modified": "2016-07-31T15:08:24.730156517Z",
	"imported": "2016-08-02T15:08:24.730156517Z",
	"name": "Test Bundle",
	"description": "A bundle for testing"
}
`)

func TestNewBundle(t *testing.T) {
	require := require.New(t)
	b, err := fb.NewBundle("krsxg5baij2w4zdmmu", "tui5ajfbabaeljnxt4om7fwmt4")
	require.Nil(err, "Error creating new bundle: %s", err)

	b.Name = "Test Bundle"
	b.Created = now
	b.Modified = now
	b.Imported = now.AddDate(0, 0, 2)
	b.Description = "A bundle for testing"
	require.Equal("bundle-tui5ajfbabaeljnxt4om7fwmt4-krsxg5baij2w4zdmmu", b.ID, "Bundle ID")
	if d := testy.DiffAsJSON(testy.Snapshot(t), b); d != nil {
		t.Errorf("New Bundle:\n%s", d)
	}

	b2 := &fb.Bundle{}
	err = json.Unmarshal(frozenBundle, b2)
	require.Nil(err, "Error thawing bundle: %s", err)
	if d := testy.DiffAsJSON(testy.Snapshot(t), b); d != nil {
		t.Errorf("Thawed Bundle:\n%s", d)
	}

	require.DeepEqual(b, b2, "Thawed vs Created bundle")
}
