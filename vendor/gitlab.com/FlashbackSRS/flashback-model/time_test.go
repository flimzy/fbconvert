package fb

import (
	"encoding/json"
	"testing"
	"time"

	"gitlab.com/flimzy/testy"
)

func parseTimeT(t *testing.T, in string) time.Time {
	tm, err := time.Parse(time.RFC3339, in)
	if err != nil {
		t.Fatal(err)
	}
	return tm
}

func TestShortTimeMarshalJSON(t *testing.T) {
	type tt struct {
		in       time.Time
		expected string
	}
	tests := testy.NewTable()
	tests.Add("standard", func(t *testing.T) interface{} {
		return tt{
			in:       parseTimeT(t, "2017-01-01T09:43:15Z"),
			expected: `"2017-01-01T09:43:15Z"`,
		}
	})
	tests.Add("truncate", func(t *testing.T) interface{} {
		return tt{
			in:       parseTimeT(t, "2017-01-01T09:43:15.987Z"),
			expected: `"2017-01-01T09:43:15Z"`,
		}
	})
	tests.Add("other tz", func(t *testing.T) interface{} {
		return tt{
			in:       parseTimeT(t, "2017-01-01T09:43:15.987+02:00"),
			expected: `"2017-01-01T07:43:15Z"`,
		}
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result, err := json.Marshal(shortTime{tt.in})
		if err != nil {
			t.Fatal(err)
		}
		if string(result) != tt.expected {
			t.Errorf(" Got: %s\nWant: %s\n", string(result), tt.expected)
		}
	})
}
