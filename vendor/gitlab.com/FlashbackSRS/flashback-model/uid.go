package fb

import (
	crypto_rand "crypto/rand"
	"encoding/binary"
	"math/rand"
)

const (
	// This gives aprox 62 bits of entropy, should be good for about 2
	// million users.
	uidLength = 12
)

const (
	uidAlphabet  = "abcdefghijklmnopqrstuvwxyz0123456789"
	uidIndexBits = 6
	uidIndexMask = 1<<uidIndexBits - 1
	uidIndexMax  = 63 / uidIndexBits
)

// Set the PRNG's seed based on CSRNG
var src = cryptoSeededSource()

func cryptoSeededSource() rand.Source {
	// borrowed from https://stackoverflow.com/a/54491783/13860
	var b [8]byte
	_, err := crypto_rand.Read(b[:])
	if err != nil {
		panic("cannot seed math/rand package with cryptographically secure random number generator: " + err.Error())
	}
	return rand.NewSource(int64(binary.LittleEndian.Uint64(b[:])))
}

// NewUID generates a new UID. It will contain at least an element of
// cryptographically-secure randomnes, and the resulting string will be safe
// for inclusion in any part of a URL. The result should be treated as an
// opaque string.
func NewUID() string {
	return randStringBytesMaskImpr(uidLength)
}

func randStringBytesMaskImpr(n int) string {
	// borrowed from https://stackoverflow.com/a/31832326/13860
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for uidIndexMax characters!
	for i, cache, remain := n-1, src.Int63(), uidIndexMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), uidIndexMax
		}
		if idx := int(cache & uidIndexMask); idx < len(uidAlphabet) {
			b[i] = uidAlphabet[idx]
			i--
		}
		cache >>= uidIndexBits
		remain--
	}

	return string(b)
}
