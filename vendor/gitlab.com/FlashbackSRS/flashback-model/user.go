package fb

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/go-kivik/kivik"
	"github.com/pkg/errors"
)

var nilUserID = "aaaaaaaaaa"

// User repressents a user of Flashback
type User struct {
	Rev      string    `json:"_rev,omitempty"`
	Name     string    `json:"name"`
	Roles    []string  `json:"roles"`
	FullName string    `json:"fullname,omitempty"`
	Email    string    `json:"email,omitempty"`
	Created  time.Time `json:"created"`
	Modified time.Time `json:"modified"`
	HasImage bool      `json:"-"`

	// Authentication details
	DerivedKey     string `json:"derived_key"`
	PasswordScheme string `json:"password_scheme"`
	Salt           string `json:"salt"`
	Iterations     int    `json:"iterations"`
}

// RandomizeID sets the user's ID randomly. It return an error if the ID is
// already set.
func (u *User) RandomizeID() error {
	if u.Name != "" {
		return errors.New("Name already set")
	}
	u.Name = NewUID()
	return nil
}

// RandomUser generates a new user with a random ID.
func RandomUser() *User {
	user := &User{}
	_ = user.RandomizeID()
	return user
}

// NewUser returns a new User object, based on the provided username.
func NewUser(name string) *User {
	return &User{
		Name:     name,
		Created:  nowUTC(),
		Modified: nowUTC(),
		Roles:    []string{},
	}
}

// GenerateUser creates a new user account, with a random ID.
func GenerateUser() *User {
	return NewUser(NewUID())
}

// ID returns the document ID for the user record.
func (u *User) ID() string {
	return kivik.UserPrefix + u.Name
}

// NilUser returns a placeholder user
func NilUser() *User {
	return NewUser(nilUserID)
}

type userAlias User

// MarshalJSON implements the json.Marshaler interface for the User type.
func (u *User) MarshalJSON() ([]byte, error) {
	type encUser struct {
		userAlias
		ID   string `json:"_id"`
		Type string `json:"type"`
		// Password must be sent to trigger server-side salt generation
		Password    *string            `json:"password,omitempty"`
		Attachments *kivik.Attachments `json:"_attachments,omitempty"`
		Created     shortTime          `json:"created"`  // nolint: govet
		Modified    shortTime          `json:"modified"` // nolint: govet
	}

	ua := userAlias(*u)
	if ua.Roles == nil {
		ua.Roles = []string{}
	}
	doc := encUser{
		userAlias: ua,
		ID:        u.ID(),
		Type:      docTypeUser,
		Created:   shortTime{u.Created},
		Modified:  shortTime{u.Modified},
	}
	if u.Salt == "" {
		// The password must be provided, but should be blank, to prevent logins
		// through the normal CouchDB API.
		doc.Password = &[]string{""}[0]
	}
	if u.HasImage {
		doc.Attachments = &kivik.Attachments{
			"picture": &kivik.Attachment{Stub: true},
		}
	}

	return json.Marshal(doc)
}

// UnmarshalJSON implements the json.Unmarshaler interface for the User type.
func (u *User) UnmarshalJSON(data []byte) error {
	type decUser struct {
		userAlias
		ID          string             `json:"_id"`
		Type        string             `json:"type"`
		Attachments *kivik.Attachments `json:"_attachments,omitempty"`
	}

	doc := new(decUser)
	if err := json.Unmarshal(data, &doc); err != nil {
		return errors.Wrap(err, "failed to unmarshal user")
	}
	if doc.Type != docTypeUser {
		return errors.New("Invalid document type for user")
	}
	if !strings.HasPrefix(doc.ID, kivik.UserPrefix) {
		return errors.New("id must have '" + kivik.UserPrefix + "' prefix")
	}
	if doc.Name != strings.TrimPrefix(doc.ID, kivik.UserPrefix) {
		return errors.New("user name and id must match")
	}
	*u = User(doc.userAlias)
	if doc.Attachments != nil {
		if doc.Attachments.Get("picture") != nil {
			u.HasImage = true
		}
	}
	return nil
}

// UsersDBID returns the document ID appropriate for the CouchDB _users db.
func (u *User) UsersDBID() string {
	return kivik.UserPrefix + u.Name
}

// DisplayName returns the user's display name.
func (u *User) DisplayName() string {
	if u.FullName != "" {
		return u.FullName
	}
	if u.Email != "" {
		return "<" + u.Email + ">"
	}
	return "{" + kivik.UserPrefix + u.Name + "}"
}
