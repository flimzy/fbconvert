package v2

import (
	"time"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

type pkg struct {
	Created  time.Time    `json:"created"`
	Modified time.Time    `json:"modified"`
	Bundle   *bundle      `json:"bundle,omitempty"`
	Cards    []*fb.Card   `json:"cards,omitempty"`
	Notes    []*fb.Note   `json:"notes,omitempty"`
	Decks    []*fb.Deck   `json:"decks,omitempty"`
	Themes   []*fb.Theme  `json:"themes,omitempty"`
	Reviews  []*fb.Review `json:"reviews,omitempty"`
}
