package v3

import (
	"encoding/json"
	"io"

	fb "gitlab.com/FlashbackSRS/flashback-model"
)

// Unpacker is the unpacker that handles v3 packages.
type Unpacker struct{}

// Unpack unpacks a version 3 package.
func (u *Unpacker) Unpack(r io.Reader) (*fb.Package, error) {
	p := new(pkg)
	if err := json.NewDecoder(r).Decode(&p); err != nil {
		return nil, err
	}
	return p.Package, p.Package.Validate()
}

type pkg struct {
	*fb.Package
}

func (p *pkg) UnmarshalJSON(data []byte) error {
	type pkgAlias pkg
	pa := new(pkgAlias)
	if err := json.Unmarshal(data, pa); err != nil {
		return err
	}
	*p = pkg(*pa)
	return nil
}
