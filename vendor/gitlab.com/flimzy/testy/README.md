Testy
------

Testy is a collection of utilities to facilitate unit testing in Go.

This software is released under the MIT license, as outlined in the included
LICENSE.md file.
