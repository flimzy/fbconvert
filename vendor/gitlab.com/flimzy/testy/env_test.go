package testy

import (
	"os"
	"sort"
	"testing"
)

func TestRestoreEnv(t *testing.T) {
	os.Clearenv()
	_ = os.Setenv("foo", "bar")
	_ = os.Setenv("bar", "baz")

	func() {
		defer RestoreEnv()()
		os.Setenv("baz", "qux")
		os.Unsetenv("bar")
		env := os.Environ()
		expected := []string{"foo=bar", "baz=qux"}
		if d := DiffInterface(expected, env); d != nil {
			t.Fatal(d)
		}
	}()

	env := os.Environ()
	expected := []string{"foo=bar", "bar=baz"}
	sort.Strings(expected)
	sort.Strings(env)
	if d := DiffInterface(expected, env); d != nil {
		t.Fatal(d)
	}
}

func TestEnviron(t *testing.T) {
	os.Clearenv()
	os.Setenv("foo", "bar")
	os.Setenv("bar", "baz")
	expected := map[string]string{
		"foo": "bar",
		"bar": "baz",
	}
	env := Environ()
	if d := DiffInterface(expected, env); d != nil {
		t.Fatal(d)
	}
}

func TestSetEnv(t *testing.T) {
	os.Clearenv()
	os.Setenv("foo", "bar")

	_ = SetEnv(map[string]string{
		"bar": "baz",
		"baz": "qux",
	})

	expected := map[string]string{
		"foo": "bar",
		"bar": "baz",
		"baz": "qux",
	}
	env := Environ()
	if d := DiffInterface(expected, env); d != nil {
		t.Fatal(d)
	}
}
