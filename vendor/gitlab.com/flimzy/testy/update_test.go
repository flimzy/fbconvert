package testy

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestUpdate(t *testing.T) {
	dir, err := ioutil.TempDir("", "diff-file")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	type updateTest struct {
		name           string
		updateMode     bool
		expected       *File
		actual         string
		diff           *Diff
		finalExpected  string
		expectedResult string
	}
	tests := []updateTest{
		{
			name:          "Non-update mode",
			updateMode:    false,
			expected:      &File{Path: "testdata/test.txt"},
			actual:        "oink",
			finalExpected: "Test Content\n",
		},
		{
			name:          "Create file",
			updateMode:    true,
			expected:      &File{Path: dir + "/create.txt"},
			diff:          &Diff{diff: "xxx"},
			actual:        "testing",
			finalExpected: "testing",
		},
		{
			name:           "Create failure",
			updateMode:     true,
			expected:       &File{Path: dir + "/foo/create.txt"},
			diff:           &Diff{diff: "xxx"},
			actual:         "testing more",
			expectedResult: "Update failed: open " + dir + "/foo/create.txt: no such file or directory",
		},
		func() updateTest {
			file := dir + "/update.txt"
			f, err := os.Create(file)
			if err != nil {
				t.Fatal(err)
			}
			_, _ = f.WriteString("something else")
			_ = f.Close()
			return updateTest{
				name:          "Ovwerrite",
				updateMode:    true,
				expected:      &File{Path: file},
				diff:          &Diff{diff: "xxx"},
				actual:        "testing update",
				finalExpected: "testing update",
			}
		}(),
		{
			name:          "No difference",
			updateMode:    true,
			expected:      &File{Path: "testdata/test.txt"},
			diff:          nil,
			actual:        "bogus text that should not be written",
			finalExpected: "Test Content",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := update(test.updateMode, test.expected, test.actual, test.diff)
			if d := DiffInterface(test.expectedResult, result.String()); d != nil {
				t.Errorf("Unexpected result:\n%s\n", d)
			}
			if result != nil {
				return
			}
			if d := DiffText(test.finalExpected, &File{Path: test.expected.Path}); d != nil {
				t.Errorf("Unexpected file contents:\n%s\n", d)
			}
		})
	}
}
